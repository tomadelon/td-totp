package org.zenika.td.totp.user;

import org.junit.jupiter.api.*;
import org.zenika.td.totp.engine.RandomKeyGenerator;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserRepositoryTest {
    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests");
    }

    @BeforeEach
    void init() {
        System.out.println("Lancement d'un test");
    }

    @Test
    void testWithUser(){
        UserRepository userRepository = new UserRepository();
        User user = new User("tomadelon@gmail.com", 1234);
        userRepository.save(user);
        assertEquals(userRepository.findByEmail("tomadelon@gmail.com"), user);

    }


}

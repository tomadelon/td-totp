package org.zenika.td.totp;

import org.zenika.td.totp.engine.RandomKeyGenerator;
import org.zenika.td.totp.engine.TotpGenerator;
import org.zenika.td.totp.service.RegisterService;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        RegisterService registerService = new RegisterService();
        TotpGenerator totpGenerator = new TotpGenerator();

        while (true) {
            System.out.println(totpGenerator.generate(registerService.register(getUserEmail())));
        }
    }
    public static String getUserEmail(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entrez votre email: ");
        String email = scanner.nextLine();
        return email;
    }
}

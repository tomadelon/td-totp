package org.zenika.td.totp.engine;

import java.util.Random;

public class RandomKeyGenerator {

    public int generateKey(){
        Random random = new Random();
        int value = random.nextInt();
        return Math.abs(value);
    }
}


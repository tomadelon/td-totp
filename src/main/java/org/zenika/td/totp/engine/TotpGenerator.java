package org.zenika.td.totp.engine;

import java.time.Instant;

public class TotpGenerator {


    public TotpGenerator(){

    }

    public long generate(long secretKey){
       long instant = Instant.now().getEpochSecond();
       long period = instant/30;
       return (secretKey+period)%1000000;
    }
}

package org.zenika.td.totp.user;

public class User {

    public String email;
    public int key;

    public User(String email, int key) {
        this.email = email;
        this.key = key;
    }

    public String getEmail() {
        return email;
    }

    public int getKey() {
        return key;
    }
}


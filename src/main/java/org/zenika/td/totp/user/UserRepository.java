package org.zenika.td.totp.user;

import java.util.HashMap;

public class UserRepository {


    HashMap<String, User> data = new HashMap<>();

    public void save(User user) {
        data.put(user.getEmail(), user);
    }

    public User findByEmail(String email){
        return data.get(email);
    }
}

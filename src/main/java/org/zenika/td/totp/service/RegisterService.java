package org.zenika.td.totp.service;


import org.zenika.td.totp.engine.RandomKeyGenerator;
import org.zenika.td.totp.user.User;
import org.zenika.td.totp.user.UserRepository;

public class RegisterService {
    RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator();
    UserRepository userRepository = new UserRepository();

  public int register(String email){
      User user = new User(email,randomKeyGenerator.generateKey());
      userRepository.save(user);
      return user.key;
  }

}
